package com.hcl.gradedAssignement;

import java.util.Scanner;

public class Main {

	
		public static void main(String[] args) {
			// TODO Auto-generated method stub
		
			SupperDepartment sd = new SupperDepartment();//creating supperdepartement objects
			AdminDepartement ad = new AdminDepartement();//creating admin departement
			HrDepartement hd = new HrDepartement();//creating hr departement
			TechDepartement td = new TechDepartement();//creating tech departement
			System.out.println();
			System.out.println(sd.departmentName());//calling the methods of supperdepartement
			System.out.println(sd.getTodaysWork());
			System.out.println(sd.getWorkDeadline());
			System.out.println(sd.isTodayHoliday());
			System.out.println();
			
			
			System.out.println(ad.departmentName());//calling the admin methods
			System.out.println(ad.getTodaysWork());
			System.out.println(ad.getWorkDeadline());
			System.out.println(ad.isTodayHoliday());
			
			System.out.println();
			
			System.out.println(hd.departmentName());//calling the hrdepartement
			System.out.println(hd.getTodaysWork());
			System.out.println(hd.getWorkDeadline());
			System.out.println(hd.doActivity());
			System.out.println(hd.isTodayHoliday());
			 
			System.out.println();
			
			System.out.println(td.departmentName());//calling tech departement methods
			System.out.println(td.getTodaysWork());
			System.out.println(td.getWorkDeadline());
			System.out.println(td.getTechStackInformation());
			System.out.println(td.isTodayHoliday()); 
			
			

		}

	
	

	}


