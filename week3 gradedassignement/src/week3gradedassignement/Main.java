package week3gradedassignement;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scanner=new Scanner(System.in);
		MagicBook mb=new MagicBook();
		while (true) {
			System.out.println("1.To add books\r\n"
					+ "2.To delete entries from book\r\n" + "3.To update a book\r\n"
					+ "4.To display all books \r\n" + "5.Total count of books\r\n"
					+ "6.Search autobiography books" +  "\n7.To display by features");

			System.out.println("Enter your choice : ");
			int choice = scanner.nextInt();

			switch (choice) {

			case 1://Adding a book
				System.out.println("Enter no of books you want to add : ");
				int n=scanner.nextInt();

				for(int i=1;i<=n;i++) {
					mb.addbook();
				}
				break;

			case 2://Deleting a book
				try {
					mb.deletebook();
				}catch(CustomerException e) {
					System.out.println(e.getMessage());
				}				
				break;
			case 3://Update a book
				try {
					mb.updatebook();
					
				}
				catch(CustomerException e)
				{
					System.out.println(e.getMessage());
				}
				break;
			case 4://displaying books
				try 
				{
					mb.displayBookInfo();
				}
				catch(CustomerException e)
				{
					
					System.out.println(e.getMessage());
				}
				break;

			case 5://counting books
				try {
					mb.count();
					
				}catch(CustomerException e){
					System.out.println(e.getMessage());
				}
		
				break;

			case 6://autobiography search
				try {
					mb.autobiography();
					
				}catch(CustomerException e) {
					System.out.println(e.getMessage());
					
				}
				
				break;


			case 7://search by feature
				System.out.println("Enter your choice : \n 1. Price low to high "
						+ "\n 2.Price high to low \n 3. Best selling");
				int ch = scanner.nextInt();

				switch (ch) {

				case 1 : try {
					mb.displayByFeature(1);
				} catch (CustomerException e) {
					// TODO Auto-generated catch block
					System.out.println(e.getMessage());
				}
				break;
				case 2:try {
					mb.displayByFeature(2);
				} catch (CustomerException e) {
					// TODO Auto-generated catch block
					System.out.println(e.getMessage());
				} 
				break;
				case 3:try {
					mb.displayByFeature(3);
				} catch (CustomerException e) {
					// TODO Auto-generated catch block
					System.out.println(e.getMessage());
				}
				break;
				}

			default:
				System.out.println("You've entered wrong choice.!");

			}

		}

	}
}


