package week3gradedassignement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;


	public class MagicBook{


		Scanner scanner=new Scanner(System.in);


		HashMap<Integer,Book> bookmap=new HashMap<>();
		TreeMap<Double,Book> treemap=new TreeMap<>();

		ArrayList<Book> booklist=new ArrayList<>();
		//adding books
		public  void addbook(){

			Book b= new Book();
			System.out.println("Enter book id : ");
			b.setId(scanner.nextInt());

			System.out.println("Enter book name : ");
			b.setName(scanner.next());

			System.out.println("Enter book price : ");
			b.setPrice(scanner.nextDouble());

			System.out.println("Enter book genre (Autobiography-Autobiography) : ");
			b.setGenre(scanner.next());

			System.out.println("Enter number of copies sold : ");
			b.setNoOfCopiesSold(scanner.nextInt());

			System.out.println("Enter book status (B-bestselling) : ");
			b.setBookstatus(scanner.next());

			bookmap.put(b.getId(), b);
			System.out.println("Book added successfully");

			treemap.put(b.getPrice(), b);
			booklist.add(b);

		}
		//deleting books
		public void deletebook() throws CustomerException{
			if(bookmap.isEmpty()) {
				throw new CustomerException("no books are available to delete!!");
			}
			else {
				System.out.println("Enter book id you want to delete : ");
				int id=scanner.nextInt();
				bookmap.remove(id);	
				System.out.println("successfully deleted!!");
			}


		}
		//updating books
		public void updatebook() throws CustomerException {
			if(bookmap.isEmpty()) {
				throw new CustomerException("No books are available to update!!");
			}else {


				Book b= new Book();
				System.out.println("Enter book id : ");
				b.setId(scanner.nextInt());

				System.out.println("Enter book name : ");
				b.setName(scanner.next());

				System.out.println("Enter book price : ");
				b.setPrice(scanner.nextDouble());

				System.out.println("Enter book genre : ");
				b.setGenre(scanner.next());

				System.out.println("Enter number of copies sold : ");
				b.setNoOfCopiesSold(scanner.nextInt());

				System.out.println("Enter book status : ");
				b.setBookstatus(scanner.next());

				bookmap.replace(b.getId(), b);
				System.out.println("Book details Updated successfully!!");
			}
		}
		//displaying books
		public void displayBookInfo() throws CustomerException {

			if (bookmap.size() > 0) 
			{
				Set<Integer> keySet = bookmap.keySet();

				for (Integer key : keySet) {

					System.out.println(key + " ----> " + bookmap.get(key));
				}
			} else {
				throw new CustomerException("BooksMap is Empty!!");
			}

		}
		//counting
		public void count() throws CustomerException {
			if(bookmap.isEmpty()) {
				throw new CustomerException("book store is empty!!");
			}else

				System.out.println("Number of books present in the store : "+bookmap.size());

		}	
		//autobiography search
		public void autobiography() throws CustomerException {
			String bestSelling = "Autobiography";
			if(booklist.isEmpty()) {
				throw new CustomerException("Book store is Empty!!");
			}
			else {

				ArrayList<Book> genreBookList = new ArrayList<Book>();

				Iterator<Book> iter=(booklist.iterator());

				while(iter.hasNext())
				{
					Book b1=(Book)iter.next();
					if(b1.genre.equals(bestSelling)) 
					{
						genreBookList.add(b1);
						System.out.println(genreBookList);
					}
				} 
			}
		}
		//displaying by feature
		public void displayByFeature(int flag) throws CustomerException{

			if(flag==1)//low to high
			{
				if (bookmap.size() > 0) 
				{
					Set<Double> keySet = treemap.keySet();
					for (Double key : keySet) {
						System.out.println(key + " ----> " + treemap.get(key));
					}
				} else {
					throw new CustomerException("book store is empty is Empty!!");
				}
			}
			if(flag==2) //high to low		
			{
				Map<Double, Object> treeMapReverseOrder= new TreeMap<>(treemap);
				// putting values in navigable map
				NavigableMap<Double, Object> nmap = ((TreeMap<Double, Object>) treeMapReverseOrder).descendingMap();

				System.out.println("Book Details : ");

				if (nmap.size() > 0) 
				{
					Set<Double> keySet = nmap.keySet();
					for (Double key : keySet) {
						System.out.println(key + " ----> " + nmap.get(key));
					}
				}else{
					throw new CustomerException("book store is Empty!!");
				}

			}

			if(flag==3) //best selling
			{
				String bestSelling = "B";
				ArrayList<Book> genreBookList = new ArrayList<Book>();

				Iterator<Book> iter=booklist.iterator();

				while(iter.hasNext())
				{
					Book b=(Book)iter.next();
					if(b.bookstatus.equals(bestSelling)) 
					{
						genreBookList.add(b);

					}

				}
				if(genreBookList.isEmpty())
				{
					throw new CustomerException("no best selling books are available!!");
				}
				else
					System.out.println("best selling books : "+genreBookList);

			}
		}	
	}