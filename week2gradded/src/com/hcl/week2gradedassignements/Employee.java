package com.hcl.week2gradedassignements;

public class Employee {
	
private int id;
private String name;
private int age;
private int salary;
private String department;
private String city;
public Employee() {
		// TODO Auto-generated constructor stub
		}
/*
 * --declared in private
 * --generated getter setter methods--
 * --we have string output so overriden tostring() method--
 * --created a constructor --- */



	public Employee(int id, String name, int age, int salary, String department, String city) {
	super();
	this.id = id;
	this.name = name;
	this.age = age;
	this.salary = salary;
	this.department = department;
	this.city = city;
}



	@Override
public String toString() {
	return "Employee [id=" + id + ", name=" + name + ", age=" + age + ", salary=" + salary + ", department="
			+ department + ", city=" + city + "]";
}

	public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public int getAge() {
	return age;
}

public void setAge(int age) {
	this.age = age;
}

public int getSalary() {
	return salary;
}

public void setSalary(int salary) {
	this.salary = salary;
}

public String getDepartment() {
	return department;
}

public void setDepartment(String department) {
	this.department = department;
}

public String getCity() {
	return city;
}

public void setCity(String city) {
	this.city = city;
}

	

	


}
