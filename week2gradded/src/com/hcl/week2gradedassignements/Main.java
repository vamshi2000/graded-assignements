package com.hcl.week2gradedassignements;

import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {
		try {
	//	ArrayList<Employee> employees = new ArrayList<>();
		Employee emp1 = new Employee();
		Employee emp2 = new Employee();
		Employee emp3 = new Employee();
		Employee emp4 = new Employee();
		Employee emp5 = new Employee();
	/*
	 * --we are setting the values for the employees ----
	 * --after that we are creating a array list of emloyees --
	 * 
	 * */
		emp1.setId(1);
		emp1.setName("vamshi");
		emp1.setAge(21);
		emp1.setSalary(3600000);
		emp1.setDepartment("IT");
		emp1.setCity("chennai");
		
				emp2.setId(2);
				emp2.setName("rk");
				emp2.setAge(22);
				emp2.setSalary(500000);
				emp2.setDepartment("HR");
				emp2.setCity("chennai");
				
				
				emp3.setId(3);
				emp3.setName("shankar");
				emp3.setAge(20);
				emp3.setSalary(750000);
				emp3.setDepartment("Admin");
				emp3.setCity("Delhi");
				
				
				emp4.setId(4);
				emp4.setName("rajesh");
				emp4.setAge(21);
				emp4.setSalary(100000);
				emp4.setDepartment("IT ");
				emp4.setCity("bangloure");
				
		
				ArrayList<Employee> employee = new ArrayList<Employee>();
				employee.add(emp1);
				employee.add(emp2);
				employee.add(emp3);
				employee.add(emp4);
			
				for(Employee empDetails:employee) {
					if(empDetails.getId()<0||empDetails.getName()==null||empDetails.getName().isEmpty()||empDetails.getAge()<0||empDetails.getSalary()<0||empDetails.getDepartment()==null||empDetails.getDepartment().isEmpty()||empDetails.getCity()==null||empDetails.getCity().isEmpty()) {
						throw new IllegalArgumentException();
					}
				}
			
				System.out.println("List of Employees : ");
				System.out.println("S.No.  Name    Age    Salary(INR)   Department Location");
				for(Employee em : employee) {
					System.out.println(em.getId() + "     " + em.getName() + "      " + em.getAge() + "     " + em.getSalary() + "      "+em.getDepartment() + "       " +em.getCity());
				}
				System.out.println();
				EmployeeNames ob1 = new 	EmployeeNames();
				ob1.sortingNames(employee);
				Cities ob2 = new 	Cities();
				ob2.cityNameCount(employee);
				EmployeeSalary ob3 = new 	EmployeeSalary();
				ob3.monthlySAlary(employee);
				
			}
	catch(IllegalArgumentException e) {
		System.out.println(e.getMessage());
	}
}
}

