package com.hcl.week2gradedassignements;

import java.util.ArrayList;
import java.util.TreeMap;

public class Cities {

		public void cityNameCount(ArrayList<Employee>employees) {//to count the number of cities we are using tree map here
			TreeMap<String,Integer>cityCount = new TreeMap<>();
			for(Employee emp : employees){
				String city = emp.getCity();
				if(cityCount.containsKey(city)) {
					cityCount.replace(city, (((int)cityCount.get(city))+1));
				}
				else
				{
					cityCount.put(city, 1);
				}
			}
			System.out.println();
			System.out.println("count of employees from each city");
			System.out.println(" " + cityCount);

	  

  }

}
