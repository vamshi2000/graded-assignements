package com.hcl.week2gradedassignements;

import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {
		ArrayList<Employee> employees = new ArrayList<>();
		Employee emp1 = new Employee();
		Employee emp2 = new Employee();
		Employee emp3 = new Employee();
		Employee emp4 = new Employee();
		Employee emp5 = new Employee();
	/*
	 * --we are setting the values for the employees ----
	 * --after that we are creating a array list of emloyees --
	 * 
	 * */
		emp1.setId(1);
		emp1.setName("vamshi");
		emp1.setAge(21);
		emp1.setSalary(3600000);
		emp1.setDepartment("IT");
		emp1.setCity("chennai");
		
				emp2.setId(2);
				emp2.setName("Bobby");
				emp2.setAge(22);
				emp2.setSalary(500000);
				emp2.setDepartment("HR");
				emp2.setCity("Bombay");
				
				
				emp3.setId(3);
				emp3.setName("Zoe");
				emp3.setAge(20);
				emp3.setSalary(750000);
				emp3.setDepartment("Admin");
				emp3.setCity("Delhi");
				
				
				emp4.setId(4);
				emp4.setName("Smitha");
				emp4.setAge(21);
				emp4.setSalary(100000);
				emp4.setDepartment("IT");
				emp4.setCity("Chennai");
				
		
				ArrayList<Employee> employee = new ArrayList<Employee>();
				employee.add(emp1);
				employee.add(emp2);
				employee.add(emp3);
				employee.add(emp4);
			
			
				System.out.println("List of Employees : ");
				System.out.println("S.No.  Name    Age    Salary(INR)   Department Location");
				for(Employee em : employee) {
					System.out.println(em.getId() + "     " + em.getName() + "      " + em.getAge() + "     " + em.getSalary() + "      "+em.getDepartment() + "       " +em.getCity());
				}
				System.out.println();
				EmployeeNames ob1 = new 	EmployeeNames();
				ob1.sortingNames(employee);
				Cities ob2 = new 	Cities();
				ob2.cityNameCount(employee);
				EmployeeSalary ob3 = new 	EmployeeSalary();
				ob3.monthlySAlary(employee);
				
			}
}

