package com.hcl.week2gradedassignements;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class EmployeeNames{

	public void sortingNames(ArrayList<Employee>employees) {
		ArrayList<String>empNames = new ArrayList<String>();
		for(Employee e : employees) {
			empNames.add(e.getName());
		}
		Collections.sort(empNames);// here we are sorting the names according to natural order
		System.out.println();
		System.out.println("Name of all the employees in sorted order are");
		System.out.println(empNames);
		
		}

}
