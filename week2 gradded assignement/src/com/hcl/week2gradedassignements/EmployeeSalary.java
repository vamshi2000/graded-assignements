package com.hcl.week2gradedassignements;

import java.util.ArrayList;
import java.util.TreeMap;

public class EmployeeSalary {
	public void monthlySAlary(ArrayList<Employee>employees) {
		try {
			TreeMap<Integer,Float>Salary_Id= new TreeMap<>();
			for(Employee e : employees) {
				Salary_Id.put(e.getId(), (float)Math.floor((float)e.getSalary()/12));
			}
			System.out.println();
			System.out.println("monthly salary of employee along with their id is ");
			System.out.println(Salary_Id);
			}
		catch(Exception exception) {
			System.out.println("exception found " + exception.getMessage());
	     }
	 }

}
